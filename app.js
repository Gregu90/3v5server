var WebSocketServer = require("ws").Server;
var http = require("http");
var express = require("express");
var port = process.env.PORT || 5000;
var CLIENTSIDS = [];
//
 var app = express();

var server = http.createServer(app);
server.listen(port);

console.log("http server listening on %d", port);

var wss = new WebSocketServer({server: server});
wss.on("connection", function (ws) {
    var userId;
    var client;
    console.info("websocket connection open");

    var timestamp = new Date().getTime();
    userId = timestamp;
    CLIENTSIDS.push(userId);
    ws.send(JSON.stringify({msgType:"onOpenConnection", msg:{connectionId:timestamp}}));


    ws.on("message", function (data, flags) {
        console.log("websocket received a message");
        console.log("message: " + data);
        //message = JSON.parse(message);
        ///Login

        // if(message.type == login) {
        //     client = User(message.user, true);
        //     client.password = message.password;
        // }
        //
        // if(message.type == name) {
        //     ws.clientName = message.data
        // }

        var clientMsg = data;

        sendAll(clientMsg, userId, ws);

    });
    ws.on("close", function () {
        console.log("websocket connection close");
    });
});
console.log("websocket server created");

function sendAll (message, id, ws) {
    wss.clients.forEach(function e(client) {
        if(client == ws) {
            client.send("Your message: " + message + " From: " + id);
        } else {
            client.send("Message received: " + message + " From: " + id);
        }
    });
}